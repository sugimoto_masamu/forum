package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "report")
public class Report {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; // 投稿ID

	@Column
	private String content; // 投稿内容

	@Column
	private Date created_date; // 投稿時間

	@Column
	private Date updated_date; // 更新時間

	// 投稿IDを取得
	public int getId() {
		return id;
	}

	// 投稿IDを格納
	public void setId(int id) {
		this.id = id;
	}

	// 投稿内容を取得
	public String getContent() {
		return content;
	}

	// 投稿内容を格納
	public void setContent(String content) {
		this.content = content;
	}

	// 投稿時間を取得
	public Date getCreated_date() {
		return created_date;
	}

	// 投稿時間を格納
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	// 更新時間を取得
	public Date getUpdated_date() {
		return updated_date;
	}

	// 更新時間を格納
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
}