package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "comment")
public class Comment {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; // 返信ID

	@Column
	private String content; // 返信内容

	@Column
	private Integer report_id; // 投稿ID

	@Column
	private Date created_date; // 投稿時間

	@Column
	private Date updated_date; // 更新時間

	// 返信IDを取得
	public int getId() {
		return id;
	}

	// 返信IDを格納
	public void setId(int id) {
		this.id = id;
	}

	// 返信内容を取得
	public String getContent() {
		return content;
	}

	// 返信内容を格納
	public void setContent(String content) {
		this.content = content;
	}

	// 投稿IDを取得
	public Integer getReport_id() {
		return report_id;
	}

	// 投稿IDを格納
	public void setReport_id(Integer report_id) {
		this.report_id = report_id;
	}

	// 投稿時間を取得
	public Date getCreated_date() {
		return created_date;
	}

	// 投稿時間を格納
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	// 更新時間を取得
	public Date getUpdated_date() {
		return updated_date;
	}

	// 更新時間を格納
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
}