package com.example.demo.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
@Transactional
public class ReportService {

	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
		public List<Report> findAllReport() {
			return reportRepository.findAll();
		}

	// レコード追加
	public void saveReport(Report report) {
		report.setCreated_date(Timestamp.valueOf(getTime()));
		report.setUpdated_date(Timestamp.valueOf(getTime()));
		reportRepository.save(report);
	}

	//レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	//レコード1件取得
	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	// 日時取得
	private String getTime() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}

	// 絞り込み
		public List<Report> findReportBetween(String startDate, String endDate) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if(startDate != "") {
				startDate += " 00:00:00";
			} else {
				startDate = "2022-01-01 00:00:00";
			}

			if(endDate != "") {
				endDate += " 23:59:59";
			} else {
				Date now = new Date();
				endDate = df.format(now);
			}

			Date start = null;
			try {
				start = df.parse(startDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Date end = null;
			try {
				end = df.parse(endDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			return reportRepository.findByCreatedDateBetween(start, end);
		}
}