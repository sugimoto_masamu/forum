package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		List<Report> contentData = reportService.findAllReport(); // 投稿を全件取得
		List<Comment> commentData = commentService.findAllComment(); // 返信を全件取得
		mav.setViewName("/top"); // 画面遷移先を指定
		mav.addObject("contents", contentData); // 投稿データオブジェクトを保管
		mav.addObject("comments", commentData); // 返信データオブジェクトを保管
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		Report report = new Report(); // form用の空のentityを準備
		mav.setViewName("/new"); // 画面遷移先を指定
		mav.addObject("formModel", report); // 準備した空のentityを保管
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		reportService.saveReport(report); // 投稿をテーブルに格納
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 返信処理
	@PostMapping("/comment")
	public ModelAndView addComment(@ModelAttribute("formModel") Comment comment) {
		commentService.saveComment(comment); // 返信をテーブルに格納
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 投稿削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id); // UrlParameterのidを基に投稿を削除
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 返信削除処理
	@DeleteMapping("/deleteComment/{id}")
	public ModelAndView deleteCommentContent(@PathVariable Integer id) {
		commentService.deleteComment(id); // UrlParameterのidを基に投稿を削除
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 投稿編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Report report = reportService.editReport(id); // 編集する投稿を取得
		mav.addObject("formModel", report); // 編集する投稿をセット
		mav.setViewName("/edit"); // 画面遷移先を指定
		return mav;
	}

	// 投稿編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		report.setId(id); // UrlParameterのidを更新するentityにセット
		reportService.saveReport(report); // 編集した投稿を更新
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 返信編集画面
	@GetMapping("/editComment/{id}")
	public ModelAndView editCommentContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Comment comment = commentService.editComment(id); // 編集する投稿を取得
		mav.addObject("formModel", comment); // 編集する投稿をセット
		mav.setViewName("/editComment"); // 画面遷移先を指定
		return mav;
	}

	// 返信編集処理
	@PutMapping("/updateComment/{id}")
	public ModelAndView updateCommentContent(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {
		comment.setId(id); // UrlParameterのidを更新するentityにセット
		comment.setReport_id(comment.getReport_id());
		commentService.saveComment(comment); // 編集した投稿を更新
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 絞り込み処理
	@GetMapping("/date")
	public ModelAndView date(@RequestParam(name = "start") String startDate,
			@RequestParam(name = "end") String endDate) {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Report> contentData = reportService.findReportBetween(startDate, endDate);
		// コメントを全件取得
		List<Comment> commentData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		// コメントデータオブジェクトを保管
		mav.addObject("comments", commentData);
		// 日付start
		mav.addObject("startDate", startDate);
		// 日付end
		mav.addObject("endDate", endDate);
		return mav;
	}
}
