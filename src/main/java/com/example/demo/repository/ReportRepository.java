package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
	@Query("select report from Report report where report.updated_date between :start and :end " + "ORDER BY report.updated_date DESC")
	List<Report> findByCreatedDateBetween(Date start, Date end);

	@Query("select report from Report report ORDER BY updated_date DESC")
	List<Report> findAll();
}